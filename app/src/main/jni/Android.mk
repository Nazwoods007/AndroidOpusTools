#указываем корневую директорию
LOCAL_PATH := $(call my-dir)

#очищаем внутренние переменные
include $(CLEAR_VARS)

#имя модуля
LOCAL_MODULE := opusfile

#С флаги для сборки кода (описание в документациях подключаемых исходников)
LOCAL_CFLAGS := -w -std=gnu99 -O2 -DNULL=0 -DSOCKLEN_T=socklen_t -DLOCALE_NOT_USED -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64 -Drestrict='' -D__EMX__ -DOPUS_BUILD -DFIXED_POINT -DUSE_ALLOCA -DHAVE_LRINT -DHAVE_LRINTF -fno-math-errno -DANDROID_NDK -DDISABLE_IMPORTGL -fno-strict-aliasing -fprefetch-loop-arrays -DAVOID_TABLES -DANDROID_TILE_BASED_DECODE -DANDROID_ARMV6_IDCT -ffast-math

#С++ флаги для сборки кода (описание в документациях подключаемых исходников)
LOCAL_CPPFLAGS := -DBSD=1 -ffast-math -O2 -funroll-loops

#подключение возможности вывода в стандартный лог андроида
LOCAL_LDLIBS := -llog

#разный режим сборки в зависимости от платформы
ifeq ($(TARGET_ARCH_ABI),armeabi)
	LOCAL_ARM_MODE  := thumb
else
	LOCAL_ARM_MODE  := arm
endif

#пути ко всем заголовочным *.h файлам
LOCAL_C_INCLUDES := \
./opus/include \
./opus/silk \
./opus/silk/fixed \
./opus/celt \
./opus \
./opus/src \
./opusfile/src \
./opusfile/include \
./ogg/include

#поиск исходников по папкам
OPUS_SRC_FILES  := $(wildcard $(LOCAL_PATH)/opus/*/*.c)
OPUS_SRC_FILES  += $(wildcard $(LOCAL_PATH)/opus/celt/*/*.c)
OPUS_SRC_FILES  += $(wildcard $(LOCAL_PATH)/opus/silk/fixed/*.c)

OPUSFILE_SRC_FILES  := $(wildcard $(LOCAL_PATH)/opusfile/src/*.c)

OGG_SRC_FILES  := $(wildcard $(LOCAL_PATH)/ogg/src/*.c)

#пути ко всем найденным *.c файлам
LOCAL_SRC_FILES     := $(OPUS_SRC_FILES)
LOCAL_SRC_FILES     += $(OPUSFILE_SRC_FILES)
LOCAL_SRC_FILES     += $(OGG_SRC_FILES)

LOCAL_SRC_FILES     += ./audio.c
LOCAL_SRC_FILES     += ./utils.c

#сборка .so библиотеки
include $(BUILD_SHARED_LIBRARY)
