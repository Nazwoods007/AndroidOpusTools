package com.terrakok.opusfile;

import java.nio.ByteBuffer;

public class OpusHelper {

    public native int startRecord(String path);

    public native int writeFrame(ByteBuffer frame, int len);

    public native void stopRecord();

    public native int openOpusFile(String path);

    public native int seekOpusFile(float position);

    public native int isOpusFile(String path);

    public native void closeOpusFile();

    public native void readOpusFile(ByteBuffer buffer, int capacity, int[] args);

    public native long getTotalPcmDuration();
    
    static {
        System.loadLibrary("opusfile");
    }
}
